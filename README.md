# SNGULAR PHP QUALITY METRICS #

## Instalación de dependencias

### Instalar composer

Se usará el método que establezca el SO o distribución, en caso de usar GNU/Linux.

* Ubuntu  
    apt-get install composer

* Fedora  
    yum install composer

* OpenSuSE  
    zypper install composer

* Mac OS X
    curl -sS https://getcomposer.org/installer | sudo php -- --install-dir=/usr/local/bin --filename=composer

* Otros sistemas  
Descargar el archivo .phar desde la url https://getcomposer.org/composer.phar e instalar con permisos de ejecución en una carpeta contenida en el PATH

### Instalar PHP Code Sniffer 

Se instala Code Sniffer globalmente usando composer con la instrucción:

    composer global require "squizlabs/php_codesniffer=*"

### Instalar PHP Mess Detector

Al igual que PHP Code Sniffer se instala globalmente usando composer:

    composer global require phpmd/phpmd

### Añadir ambos comandos al PATH

Ejecutar la siguiente orden:

    echo -n "export PATH=\$PATH:~/.config/composer/vendor/bin:~/.composer/vendor/bin" >> ~/.bashrc

    * Mac OS X
        echo -n "export PATH=\$PATH:~/.composer/vendor/bin" >> ~/.profile

En caso de fallo, añadir manualmente al archivo .bashrc la línea:

    export PATH=$PATH:~/.config/composer/vendor/bin:~/.composer/vendor/bin

## Instalación de la reglas de PHP Mess Detector

Copiar el contenido de la carpeta rulesets a

    .config/composer/vendor/phpmd/phpmd/src/main/resources/rulesets

En algunos sistemas, el directorio donde residen los archivos de composer se encuentran en otro lugar, en ese caso, se deben copiar los archivos anteriores a

    .composer/vendor/phpmd/phpmd/src/main/resources/rulesets

## Instalación del hook

Copiar el contenido de la carpeta hooks en la carpeta hooks/ del directory .git

Ahora, cada vez que se ejecute un git commit, se realizarán las comprobaciones necesarias.

También es posible realizar comprobaciones manuales a un archivo ejecutando:  

    phpcs --standard=PSR2 --extensions=php --encoding=utf8 --report=full path/to/code  
    phpmd path/to/code text reglas-a-aplicar --suffixes=php --ignore-violations-on-exit

Sustituyendo reglas-a-aplicar for sng-critical, sng-warnigs o ambas, dependiendo de qué se quiera comprobar.
